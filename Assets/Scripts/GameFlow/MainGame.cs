using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Entities;
using Assets.Scripts.Modules;

namespace Assets.Scripts.GameFlow
{
    public class MainGame : MonoBehaviour
    {
        /* ---------------------------------------------------------------------
         * ATTRIBUTES
         * ------------------------------------------------------------------ */
        /* ---- ENTITIES ---- */
        private Player player;
        private List<Chest> chestList;

        /* ---- MODULES ---- */
        private CameraHandler camHandler;
        private CollisionHandler collHandler;

        /* ---- VARIABLES FOR INTEGRATION ---- */
        private float accumulatedTime = 0.0f;
        private float timeStep = 0.1f;

        /* ---------------------------------------------------------------------
         * METHODS
         * ------------------------------------------------------------------ */
        /* ---- INITIALISATION ---- */
        void Start()
        {
            /* Create entities */
            player = new Player(216, 120);
            chestList = new List<Chest>();

            /* Create modules */
            camHandler = new CameraHandler();
            collHandler = new CollisionHandler();

            /* Populate lists */
            chestList.Add(new Chest(100, 120));
        }

        /* ---- UPDATE (ONCE PER FRAME) ---- */
        void Update()
        {
            /* Do AI */

            /* Handle inputs */
            player.HandleInput();

            /* Integrate physics */
            accumulatedTime += Time.deltaTime;
            while(accumulatedTime >= timeStep)
            {
                /* Handle collisions */
                collHandler.checkChests(chestList, player, timeStep);

                /* Propagate states */
                player.UpdateState(timeStep);

                /* Update accumulated time */
                accumulatedTime -= timeStep;
            }

            /* Update camera position */
            camHandler.Update(player);

            /* Render */
            player.Render();
        }
    }
}

