using UnityEngine;
using System.Collections;


namespace Assets.Scripts.Entities
{
    public class Enemy : Character
    {
        /* ---------------------------------------------------------------------
         * ATTRIBUTES
         * ------------------------------------------------------------------ */
        /* ---- CONSTANTS TO BE STORED ---- */
        /* Walking speed */
        private static float WALK_SPEED = 70.0f;

        /* Running speed */
        private static float RUN_SPEED = 120.0f;

        /* Jump initial speed */
        private static float JUMP_INIT_SPEED = 160.0f;

        /* Gravity acceleration */
        private static float GRAVITY = 400.0f;

        /* Collider position with respect to teh enemy's */
        private static float HOR_COLL_POS = 0.0f;
        private static float VER_COLL_POS = -10.0f;

        /* Collider size */
        private static float COLL_WIDTH = 20.0f;
        private static float COLL_HEIGHT = 16.0f;

        /* ---- RENDER VARIABLES ---- */
        tk2dSpriteAnimator enemyAnim;
        tk2dSpriteAnimator shadowAnim;

        /* ---------------------------------------------------------------------
         * METHODS
         * ------------------------------------------------------------------ */ 
        /* ---- INSTANTIATION ---- */
        public Enemy(float horInitPos, float verInitPos) :
            base(WALK_SPEED, RUN_SPEED, JUMP_INIT_SPEED, GRAVITY,
                 horInitPos, verInitPos,
                 HOR_COLL_POS, VER_COLL_POS,
                 COLL_WIDTH, COLL_HEIGHT)
        {
            /* Initialise enemy's animation */
            enemyAnim = GameObject.Find("EnemyAnimReference").
                GetComponent<tk2dSpriteAnimator>();
            enemyAnim.transform.position =
                new Vector3(horInitPos, verInitPos, verInitPos);
            enemyAnim.Play("IdleDown");

            /* Initialise shadow's animation */
            shadowAnim = GameObject.Find("ShadowAnimReference").
                GetComponent<tk2dSpriteAnimator>();
            shadowAnim.transform.position =
                new Vector3(horInitPos, verInitPos, verInitPos + 1);
            shadowAnim.Play("Idle");
        }
        
        /* ---- RENDER ---- */
        public void Render()
        {
            /* Update positions of enemy and shadow */
            enemyAnim.transform.position =
                new Vector3(HorPos, VerPos + JumpPos, VerPos);
            shadowAnim.transform.position =
                new Vector3(HorPos, VerPos, VerPos + 1);

            /* Decide which clip to play */
            switch(Facing)
            {
                case DirectionFacing.Down:
                    if(IsJumping)
                    {
                        enemyAnim.Play("JumpDown");
                    }
                    else if(IsAttacking)
                    {
                        enemyAnim.Play("AttackDown");
                    }
                    else if(IsRunning)
                    {
                        enemyAnim.Play("RunDown");
                    }
                    else if(IsWalking)
                    {
                        enemyAnim.Play("WalkDown");
                    }
                    else
                    {
                        enemyAnim.Play("IdleDown");
                    }
                    enemyAnim.Sprite.FlipX = false;
                    break;
                case DirectionFacing.Left:
                    if(IsJumping)
                    {
                        enemyAnim.Play("JumpRight");
                    }
                    else if(IsAttacking)
                    {
                        enemyAnim.Play("AttackRight");
                    }
                    else if(IsRunning)
                    {
                        enemyAnim.Play("RunRight");
                    }
                    else if(IsWalking)
                    {
                        enemyAnim.Play("WalkRight");
                    }
                    else
                    {
                        enemyAnim.Play("IdleRight");
                    }
                    enemyAnim.Sprite.FlipX = true;
                    break;
                case DirectionFacing.Up:
                    if(IsJumping)
                    {
                        enemyAnim.Play("JumpUp");
                    }
                    else if(IsAttacking)
                    {
                        enemyAnim.Play("AttackUp");
                    }
                    else if(IsRunning)
                    {
                        enemyAnim.Play("RunUp");
                    }
                    else if(IsWalking)
                    {
                        enemyAnim.Play("WalkUp");
                    }
                    else
                    {
                        enemyAnim.Play("IdleUp");
                    }
                    enemyAnim.Sprite.FlipX = false;
                    break;
                case DirectionFacing.Right:
                    if(IsJumping)
                    {
                        enemyAnim.Play("JumpRight");
                    }
                    else if(IsAttacking)
                    {
                        enemyAnim.Play("AttackRight");
                    }
                    else if(IsRunning)
                    {
                        enemyAnim.Play("RunRight");
                    }
                    else if(IsWalking)
                    {
                        enemyAnim.Play("WalkRight");
                    }
                    else
                    {
                        enemyAnim.Play("IdleRight");
                    }
                    enemyAnim.Sprite.FlipX = false;
                    break;
            }

            enemyAnim.AnimationCompleted = ChooseNextState;
        }

        /* ---- HANDLE END OF ANIMATION ---- */
        public void ChooseNextState
           (tk2dSpriteAnimator x, tk2dSpriteAnimationClip y)
        {
            if(IsAttacking)
            {
                IsAttacking = false;
            }
        }
    }
}
