﻿using UnityEngine;
using System.Collections;


namespace Assets.Scripts.Entities
{
    public class ColliderBox
    {
        /* ---------------------------------------------------------------------
         * ATTRIBUTES
         * ------------------------------------------------------------------ */
        /* ---- PHYSIC VARIABLES ---- */
        /* Position with respect to owner */
        private float _horOffset, _verOffset;

        /* Size of the collider */
        private float _width, _height;

        /* ---------------------------------------------------------------------
         * METHODS
         * ------------------------------------------------------------------ */
        /* ---- INSTANTIATION ---- */
        public ColliderBox
           (float horOffset, float verOffset, float width, float height)
        {
            _horOffset = horOffset;
            _verOffset = verOffset;
            _width = width;
            _height = height;
        }

        /* ---------------------------------------------------------------------
         * GETTERS AND SETTERS
         * ------------------------------------------------------------------ */
        /* Only getters, only way to set is at creation */
        public float HorOffset
        {
            get { return _horOffset; }
        }
        public float VerOffset
        {
            get { return _verOffset; }
        }
        public float Width
        {
            get { return _width; }
        }
        public float Height
        {
            get { return _height; }
        }
    }
}
