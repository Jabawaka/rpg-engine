﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities
{
    /* ---- NAMESPACE ENUMS ---- */
    /* Direction that the Entity faces */
    public enum DirectionFacing
    {
        Down,
        Right,
        Up,
        Left
    }


    public class Character
    {
        /* ---------------------------------------------------------------------
         * ATRIBUTES
         * ------------------------------------------------------------------ */
        /* ---- CONSTANT PROPERTIES ---- */
        /* Walking speed */
        private float _walkSpeed;

        /* Running speed */
        private float _runSpeed;

        /* Jump initial speed */
        private float _jumpIniSpeed;

        /* Gravity acceleration */
        private float _gravity;

        /* ---- PHYSIC VARIABLES ---- */
        /* Position in the global frame */
        private float _horPos, _verPos;

        /* Speed in the global frame */
        private float _horSpeed, _verSpeed;

        /* Jumping position and speed */
        private float _jumpPos, _jumpSpeed;

        /* Collider box */
        private ColliderBox _collBox;

        /* ---- STATUS VARIABLES ---- */
        /* Direction the character faces */
        private DirectionFacing _facing;

        /* Booleans, self-explanatory */
        private bool _isWalking;
        private bool _isJumping;
        private bool _isRunning;
        private bool _isAttacking;

        /* ---------------------------------------------------------------------
         * METHODS
         * ------------------------------------------------------------------ */
        /* ---- INSTANTIATION ---- */
        public Character
           (float walkSpeed, float runSpeed, float jumpSpeed, float gravity,
            float horInitPos, float verInitPos,
            float horCollPos, float verCollPos,
            float collWidth, float collHeight)
        {
            /* Set property constants */
            _walkSpeed = walkSpeed;
            _runSpeed = runSpeed;
            _jumpIniSpeed = jumpSpeed;
            _gravity = gravity;

            /* Set physic variables */
            _horPos = horInitPos;
            _verPos = verInitPos;
            _horSpeed = 0.0f;
            _verSpeed = 0.0f;
            _jumpPos = 0.0f;
            _jumpSpeed = 0.0f;

            /* Set collider */
            _collBox = new ColliderBox
               (horCollPos, verCollPos, collWidth, collHeight);

            /* Set status */
            _facing = DirectionFacing.Down;
            _isWalking = false;
            _isJumping = false;
            _isRunning = false;
            _isAttacking = false;
        }

        /* ---- UPDATE METHOD ---- */
        public void UpdateState(float timeStep)
        {
            /* Integrate jumping */
            if(_isJumping)
            {
                _jumpSpeed -= _gravity * timeStep;
                _jumpPos += _jumpSpeed * timeStep;
            }

            if(_jumpPos <= 0.0f)
            {
                _jumpSpeed = 0.0f;
                _jumpPos = 0.0f;
                _isJumping = false;
            }

            /* Integrate position */
            _horPos += _horSpeed * timeStep;
            _verPos += _verSpeed * timeStep;
        }

        /* ---------------------------------------------------------------------
         * GETTERS AND SETTERS
         * ------------------------------------------------------------------ */
        /* ---- PROPERTIES ---- */
        public float WalkSpeed
        {
            get { return _walkSpeed; }
        }
        public float RunSpeed
        {
            get { return _runSpeed; }
        }
        public float JumpIniSpeed
        {
            get { return _jumpIniSpeed; }
        }

        /* ---- PHYSIC VARIABLES ---- */
        public float HorPos
        {
            get { return _horPos; }
            set { _horPos = value; }
        }
        public float VerPos
        {
            get { return _verPos; }
            set { _verPos = value; }
        }
        public float HorSpeed
        {
            get { return _horSpeed; }
            set { _horSpeed = value; }
        }
        public float VerSpeed
        {
            get { return _verSpeed; }
            set { _verSpeed = value; }
        }
        public float JumpSpeed
        {
            get { return _jumpSpeed; }
            set { _jumpSpeed = value; }
        }
        public float JumpPos
        {
            get { return _jumpPos; }
            set { _jumpPos = value; }
        }
        public ColliderBox CollBox
        {
            get { return _collBox; }
            set { _collBox = value; }
        }
        
        /* ---- STATUS VARIABLES ---- */
        public DirectionFacing Facing
        {
            get { return _facing; }
            set { _facing = value; }
        }
        public bool IsWalking
        {
            get { return _isWalking; }
            set { _isWalking = value; }
        }
        public bool IsRunning
        {
            get { return _isRunning; }
            set { _isRunning = value; }
        }
        public bool IsJumping
        {
            get { return _isJumping; }
            set { _isJumping = value; }
        }
        public bool IsAttacking
        {
            get { return _isAttacking; }
            set { _isAttacking = value; }
        }
    }
}
