﻿using UnityEngine;
using System.Collections;


namespace Assets.Scripts.Entities
{
    public enum PossibleInputs
    {
        Idle,
        Down,
        Right,
        Up,
        Left,
        DownRight,
        RightUp,
        UpLeft,
        LeftDown,
        Jump,
        Attack
    }

    public class Player : Character
    {
        /* ---------------------------------------------------------------------
         * ATTRIBUTES
         * ------------------------------------------------------------------ */
        /* ---- CONSTANTS TO BE STORED ---- */
        /* Walking speed */
        private static float WALK_SPEED = 70.0f;

        /* Running speed */
        private static float RUN_SPEED = 120.0f;

        /* Jump initial speed */
        private static float JUMP_INIT_SPEED = 160.0f;

        /* Gravity acceleration */
        private static float GRAVITY = 400.0f;

        /* Collider position with respect to the player's */
        private static float HOR_COLL_POS = 0.0f;
        private static float VER_COLL_POS = -10.0f;

        /* Collider size */
        private static float COLL_WIDTH = 20.0f;
        private static float COLL_HEIGHT = 16.0f;

        /* Tap time threshold for combos */
        private static float TAP_TIME_THRESHOLD = 0.5f;

        /* ---- INPUT VARIABLES ---- */
        /* Last key pressed */
        private PossibleInputs _lastInput = PossibleInputs.Idle;

        /* Previous key tapped (from idle to active input) */
        private PossibleInputs _prevTap = PossibleInputs.Idle;

        /* Last key tapped */
        private PossibleInputs _lastTap = PossibleInputs.Idle;

        /* Number of taps since last combo was consumed */
        private int _tapCount;

        /* Time of the previous tap ocurrence */
        private float _prevTapTime = 0.0f;

        /* Time ellapsed until current tap */
        private float _tapTimeEllapsed = 1.0f;

        /* ---- ADDITIONAL STATUS VARIABLES ---- */
        private bool _isActionPerformed = false;

        /* ---- RENDER VARIABLES ---- */
        tk2dSpriteAnimator playerAnim;
        tk2dSpriteAnimator shadowAnim;

        /* ---------------------------------------------------------------------
         * METHODS
         * ------------------------------------------------------------------ */
        /* ---- INSTANTIATION ---- */
        public Player(float horInitPos, float verInitPos) :
            base(WALK_SPEED, RUN_SPEED, JUMP_INIT_SPEED, GRAVITY,
                 horInitPos, verInitPos,
                 HOR_COLL_POS, VER_COLL_POS,
                 COLL_WIDTH, COLL_HEIGHT)
        {
            /* Initialise player's animation */
            playerAnim = GameObject.Find("PlayerAnimReference").
                GetComponent<tk2dSpriteAnimator>();
            playerAnim.transform.position =
                new Vector3(horInitPos, verInitPos, verInitPos);
            playerAnim.Play("IdleDown");

            /* Initialise shadow's animation */
            shadowAnim = GameObject.Find("ShadowAnimReference").
                GetComponent<tk2dSpriteAnimator>();
            shadowAnim.transform.position =
                new Vector3(horInitPos, verInitPos, verInitPos + 1);
            shadowAnim.Play("Idle");
        }

        /* ---- HANDLE INPUT ---- */
        public void HandleInput()
        {
            /* Handle all direction inputs if not jumping or attacking */
            if(!IsJumping && !IsAttacking)
            {
                HandleDirectionalInputs();
            }

            /* Jump button */
            if(Input.GetButtonDown("Jump") && !IsJumping)
            {
                IsJumping = true;
                JumpSpeed = JumpIniSpeed;
            }

            /* Run button */
            if(Input.GetButtonDown("Run"))
            {
                IsRunning = true;
            }

            /* Attack button */
            if(Input.GetButtonDown("Attack 1"))
            {
                IsAttacking = true;
                HorSpeed = 0.0f;
                VerSpeed = 0.0f;
                IsWalking = false;
                IsRunning = false;
            }

            /* Action button */
            if(Input.GetButtonDown("Action"))
            {
                _isActionPerformed = true;
            }

            /* Handle all combos */
            HandlePlayerCombos();
        }

        /* ---- RENDER ---- */
        public void Render()
        {
            /* Update positions of player and shadow */
            playerAnim.transform.position =
                new Vector3(HorPos, VerPos + JumpPos, VerPos);
            shadowAnim.transform.position =
                new Vector3(HorPos, VerPos, VerPos + 1);

            /* Decide which clip to play */
            switch(Facing)
            {
                case DirectionFacing.Down:
                    if(IsJumping)
                    {
                        playerAnim.Play("JumpDown");
                    }
                    else if(IsAttacking)
                    {
                        playerAnim.Play("AttackDown");
                    }
                    else if(IsRunning)
                    {
                        playerAnim.Play("RunDown");
                    }
                    else if(IsWalking)
                    {
                        playerAnim.Play("WalkDown");
                    }
                    else
                    {
                        playerAnim.Play("IdleDown");
                    }
                    playerAnim.Sprite.FlipX = false;
                    break;
                case DirectionFacing.Left:
                    if(IsJumping)
                    {
                        playerAnim.Play("JumpRight");
                    }
                    else if(IsAttacking)
                    {
                        playerAnim.Play("AttackRight");
                    }
                    else if(IsRunning)
                    {
                        playerAnim.Play("RunRight");
                    }
                    else if(IsWalking)
                    {
                        playerAnim.Play("WalkRight");
                    }
                    else
                    {
                        playerAnim.Play("IdleRight");
                    }
                    playerAnim.Sprite.FlipX = true;
                    break;
                case DirectionFacing.Up:
                    if(IsJumping)
                    {
                        playerAnim.Play("JumpUp");
                    }
                    else if(IsAttacking)
                    {
                        playerAnim.Play("AttackUp");
                    }
                    else if(IsRunning)
                    {
                        playerAnim.Play("RunUp");
                    }
                    else if(IsWalking)
                    {
                        playerAnim.Play("WalkUp");
                    }
                    else
                    {
                        playerAnim.Play("IdleUp");
                    }
                    playerAnim.Sprite.FlipX = false;
                    break;
                case DirectionFacing.Right:
                    if(IsJumping)
                    {
                        playerAnim.Play("JumpRight");
                    }
                    else if(IsAttacking)
                    {
                        playerAnim.Play("AttackRight");
                    }
                    else if(IsRunning)
                    {
                        playerAnim.Play("RunRight");
                    }
                    else if(IsWalking)
                    {
                        playerAnim.Play("WalkRight");
                    }
                    else
                    {
                        playerAnim.Play("IdleRight");
                    }
                    playerAnim.Sprite.FlipX = false;
                    break;
            }

            playerAnim.AnimationCompleted = ChooseNextState;
        }

        /* ---- HANDLE END OF ANIMATION ---- */
        public void ChooseNextState
           (tk2dSpriteAnimator x, tk2dSpriteAnimationClip y)
        {
            if(IsAttacking)
            {
                IsAttacking = false;
            }
        }
        
        /* ---- HANDLE DIRECTIONAL INPUT ---- */
        private void HandleDirectionalInputs()
        {
            /* Variables to store possible future state */
            float horNextSpeed = 0.0f;
            float verNextSpeed = 0.0f;

            /* Right pressed */
            if(Input.GetAxisRaw("Horizontal") > 0.0f ||
                Input.GetAxisRaw("DPadHor") > 0.0f)
            {
                /* Set walking or running speed */
                horNextSpeed = WalkSpeed;
                if(IsRunning && Facing == DirectionFacing.Right)
                {
                    horNextSpeed = RunSpeed;
                }
                
                /* Update input info */
                if(_lastInput == PossibleInputs.Idle)
                {
                    _tapTimeEllapsed = Time.time - _prevTapTime;
                    _prevTapTime = Time.time;
                    _tapCount++;
                    _prevTap = _lastTap;
                    _lastTap = PossibleInputs.Right;
                }
                _lastInput = PossibleInputs.Right;
            }

            /* Left pressed */
            if(Input.GetAxisRaw("Horizontal") < 0.0f ||
                Input.GetAxisRaw("DPadHor") < 0.0f)
            {
                /* Set walking or running speed */
                horNextSpeed = -WalkSpeed;
                if(IsRunning && Facing == DirectionFacing.Left)
                {
                    horNextSpeed = -RunSpeed;
                }

                /* Update input info */
                if(_lastInput == PossibleInputs.Idle)
                {
                    _tapTimeEllapsed = Time.time - _prevTapTime;
                    _prevTapTime = Time.time;
                    _tapCount++;
                    _prevTap = _lastTap;
                    _lastTap = PossibleInputs.Left;
                }
                _lastInput = PossibleInputs.Left;
            }

            /* Up pressed */
            if(Input.GetAxisRaw("Vertical") > 0.0f ||
                Input.GetAxisRaw("DPadVer") > 0.0f)
            {
                /* Set walking or running speed */
                verNextSpeed = WalkSpeed;
                if(IsRunning && Facing == DirectionFacing.Up)
                {
                    verNextSpeed = RunSpeed;
                }

                /* Update input info */
                if(_lastInput == PossibleInputs.Idle)
                {
                    _tapTimeEllapsed = Time.time - _prevTapTime;
                    _prevTapTime = Time.time;
                    _tapCount++;
                    _prevTap = _lastTap;
                    _lastTap = PossibleInputs.Up;
                }
                _lastInput = PossibleInputs.Up;
            }

            /* Down pressed */
            if(Input.GetAxisRaw("Vertical") < 0.0f ||
                Input.GetAxisRaw("DPadVer") < 0.0f)
            {
                /* Set walking or running speed */
                verNextSpeed = -WalkSpeed;
                if(IsRunning && Facing == DirectionFacing.Down)
                {
                    verNextSpeed = -RunSpeed;
                }

                /* Update input info */
                if(_lastInput == PossibleInputs.Idle)
                {
                    _tapTimeEllapsed = Time.time - _prevTapTime;
                    _prevTapTime = Time.time;
                    _tapCount++;
                    _prevTap = _lastTap;
                    _lastTap = PossibleInputs.Down;
                }
                _lastInput = PossibleInputs.Down;
            }

            /* No directional pressed */
            if(horNextSpeed == 0.0f && verNextSpeed == 0.0f)
            {
                _lastInput = PossibleInputs.Idle;
            }

            /* Choose direction of facing: this must be done here because the
             * character is walking on a 2D plane and therefore the animation
             * might change or not depending on current state and input */
            if(IsWalking)
            {
                if(verNextSpeed == 0.0f)
                {
                    if(horNextSpeed == 0.0f)
                    {
                        IsWalking = false;
                        IsRunning = false;
                    }
                    else if(horNextSpeed > 0.0f)
                    {
                        Facing = DirectionFacing.Right;
                    }
                    else if(horNextSpeed < 0.0f)
                    {
                        Facing = DirectionFacing.Left;
                    }
                }
                else if(verNextSpeed > 0.0f)
                {
                    if(horNextSpeed == 0.0f || HorSpeed == 0.0f)
                    {
                        Facing = DirectionFacing.Up;
                    }
                    else if(horNextSpeed > 0.0f)
                    {
                        if(VerSpeed == 0.0f)
                        {
                            Facing = DirectionFacing.Right;
                        }
                        else if(HorSpeed < 0.0f)
                        {
                            Facing = DirectionFacing.Up;
                        }
                        else if(VerSpeed < 0.0f &&
                            Facing == DirectionFacing.Down)
                        {
                            Facing = DirectionFacing.Up;
                        }
                    }
                    else if(horNextSpeed < 0.0f)
                    {
                        if(VerSpeed == 0.0f)
                        {
                            Facing = DirectionFacing.Left;
                        }
                        else if(HorSpeed > 0.0f)
                        {
                            Facing = DirectionFacing.Up;
                        }
                        else if(VerSpeed < 0.0f &&
                            Facing == DirectionFacing.Down)
                        {
                            Facing = DirectionFacing.Up;
                        }
                    }
                }
                else if(verNextSpeed < 0.0f)
                {
                    if(horNextSpeed == 0.0f || HorSpeed == 0.0f)
                    {
                        Facing = DirectionFacing.Down;
                    }
                    else if(horNextSpeed > 0.0f)
                    {
                        if(VerSpeed == 0.0f)
                        {
                            Facing = DirectionFacing.Right;
                        }
                        else if(HorSpeed < 0.0f)
                        {
                            Facing = DirectionFacing.Down;
                        }
                        else if(VerSpeed > 0.0f &&
                            Facing == DirectionFacing.Up)
                        {
                            Facing = DirectionFacing.Down;
                        }
                    }
                    else if(horNextSpeed < 0.0f)
                    {
                        if(VerSpeed == 0.0f)
                        {
                            Facing = DirectionFacing.Left;
                        }
                        else if(HorSpeed > 0.0f)
                        {
                            Facing = DirectionFacing.Down;
                        }
                        else if(VerSpeed > 0.0f &&
                            Facing == DirectionFacing.Up)
                        {
                            Facing = DirectionFacing.Down;
                        }
                    }
                }
            }
            else
            {
                /* Normal logic */
                if(horNextSpeed > 0.0f)
                {
                    IsWalking = true;
                    Facing = DirectionFacing.Right;
                }
                else if(horNextSpeed < 0.0f)
                {
                    IsWalking = true;
                    Facing = DirectionFacing.Left;
                }
                if(verNextSpeed > 0.0f)
                {
                    IsWalking = true;
                    Facing = DirectionFacing.Up;
                }
                if(verNextSpeed < 0.0f)
                {
                    IsWalking = true;
                    Facing = DirectionFacing.Down;
                }
            }

            /* Turn off running when turning on the spot */
            if(IsRunning)
            {
                if(horNextSpeed * HorSpeed < 0.0f ||
                    verNextSpeed * VerSpeed < 0.0f)
                {
                    IsRunning = false;
                }
            }

            /* Finally, set state */
            HorSpeed = horNextSpeed;
            VerSpeed = verNextSpeed;
        }

        /* ---- HANDLE PLAYER COMBOS ---- */
        public void HandlePlayerCombos()
        {
            if(_prevTap == _lastTap &&
                _tapTimeEllapsed <= TAP_TIME_THRESHOLD &&
                _tapCount >= 2)
            {
                IsRunning = true;
                _tapCount = 0;
            }
        }

        /* ---------------------------------------------------------------------
         * GETTERS AND SETTERS
         * ------------------------------------------------------------------ */
        public bool IsActionPerf
        {
            get { return _isActionPerformed; }
            set { _isActionPerformed = value; }
        }
    }
}
