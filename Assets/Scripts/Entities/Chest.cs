﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities
{
    public class Chest
    {
        /* ---------------------------------------------------------------------
         * ATTRIBUTES
         * ------------------------------------------------------------------ */
        /* ---- PHYSICS COLLIDER CONSTANTS ---- */
        private static float PHYS_COLL_HOR_OFFSET = 0.0f;
        private static float PHYS_COLL_VER_OFFSET = -3.0f;
        private static float PHYS_COLL_WIDTH = 16.0f;
        private static float PHYS_COLL_HEIGHT = 8.0f;

        /* ---- ACTION COLLIDER CONSTANTS ---- */
        private static float ACT_COLL_HOR_OFFSET = 0.0f;
        private static float ACT_COLL_VER_OFFSET = -15.0f;
        private static float ACT_COLL_WIDTH = 16.0f;
        private static float ACT_COLL_HEIGHT = 6.0f;

        /* ---- RENDER VARIABLES ---- */
        private tk2dSpriteAnimator _chestAnim;

        /* ---- PHYSICS VARIABLES ---- */
        /* Position in the global map */
        private float _horPos, _verPos;

        /* Physics collider box */
        private ColliderBox _physBox;

        /* Action collider box */
        private ColliderBox _actBox;

        /* ---- STATUS VARIABLES ---- */
        private bool _isOpen;

        /* ---------------------------------------------------------------------
         * METHODS
         * ------------------------------------------------------------------ */
        /* ---- INSTANTIATION ---- */
        public Chest(float horPos, float verPos)
        {
            _horPos = horPos;
            _verPos = verPos;
            _physBox = new ColliderBox
               (PHYS_COLL_HOR_OFFSET, PHYS_COLL_VER_OFFSET,
                PHYS_COLL_WIDTH, PHYS_COLL_HEIGHT);
            _actBox = new ColliderBox
               (ACT_COLL_HOR_OFFSET, ACT_COLL_VER_OFFSET,
                ACT_COLL_WIDTH, ACT_COLL_HEIGHT);

            _isOpen = false;

            _chestAnim = GameObject.Find("ChestAnimReference").
                GetComponent<tk2dSpriteAnimator>();
            _chestAnim.transform.position =
                new Vector3(horPos, verPos, verPos);
            _chestAnim.Play("IdleClosed");
        }

        /* ---- OPEN CHEST ---- */
        public void OpenChest()
        {
            _isOpen = true;
            _chestAnim.Play("IdleOpen");
        }

        /* ---------------------------------------------------------------------
         * GETTERS AND SETTERS
         * ------------------------------------------------------------------ */
        public float HorPos
        {
            get { return _horPos; }
            set { _horPos = value; }
        }
        public float VerPos
        {
            get { return _verPos; }
            set { _verPos = value; }
        }
        public bool IsOpen
        {
            get { return _isOpen; }
            set { _isOpen = value; }
        }
        public ColliderBox PhysBox
        {
            get { return _physBox; }
            set { _physBox = value; }
        }
        public ColliderBox ActBox
        {
            get { return _actBox; }
            set { _actBox = value; }
        }
    }
}
