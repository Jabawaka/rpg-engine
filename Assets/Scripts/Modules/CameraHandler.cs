using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;

namespace Assets.Scripts.Modules
{
    public class CameraHandler
    {
        /* ---------------------------------------------------------------------
         * ATTRIBUTES
         * ------------------------------------------------------------------ */
        /* ---- CONSTANTS ---- */
        /* Maximum distance to the center the player can have in each axis */
        private static int HOR_MAX_OFFSET = 43;
        private static int VER_MAX_OFFSET = 24;

        /* Position "out of the screen" of the camera */
        private static int Z_POSITION = 0;

        /* ---- VARIABLES ---- */
        /* Position of the lower left corner of the camera focus */
        private float _horPos, _verPos;

        /* Camera instance */
        private tk2dCamera _camera;

        /* ---------------------------------------------------------------------
         * METHODS
         * ------------------------------------------------------------------ */
        /* ---- INSTANTIATION ---- */
        public CameraHandler()
        {
            _horPos = 0.0f;
            _verPos = 0.0f;
            _camera = GameObject.Find("MainCamera").GetComponent<tk2dCamera>();
        }

        /* ---- UPDATE POSITION ---- */
        public void Update(Player player)
        {
            float horPosPlayer_cam, verPosPlayer_cam;

            horPosPlayer_cam = player.HorPos -
               (_horPos + _camera.nativeResolutionWidth / 2);
            verPosPlayer_cam = player.VerPos -
               (_verPos + _camera.nativeResolutionHeight / 2);

            if(Mathf.Abs(horPosPlayer_cam) > HOR_MAX_OFFSET)
            {
                _horPos += horPosPlayer_cam -
                    Mathf.Sign(horPosPlayer_cam) * HOR_MAX_OFFSET;
            }

            if(Mathf.Abs(verPosPlayer_cam) > VER_MAX_OFFSET)
            {
                _verPos += verPosPlayer_cam -
                    Mathf.Sign(verPosPlayer_cam) * VER_MAX_OFFSET;
            }

            /* Set new position */
            _camera.transform.position =
                new Vector3(_horPos, _verPos, Z_POSITION);
        }

        /* ---------------------------------------------------------------------
         * GETTERS AND SETTERS
         * ------------------------------------------------------------------ */
        public float HorPos
        {
            get { return _horPos; }
            set { _horPos = value; }
        }
        public float VerPos
        {
            get { return _verPos; }
            set { _verPos = value; }
        }
    }
}
