using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Entities;

namespace Assets.Scripts.Modules
{
    public class CollisionHandler
    {
        /* ---------------------------------------------------------------------
         * ATTRIBUTES
         * ------------------------------------------------------------------ */

        /* ---------------------------------------------------------------------
         * METHODS
         * ------------------------------------------------------------------ */
        /* ---- INSTANTIATION ---- */
        public CollisionHandler()
        {
        }

        /* ---- CHECK ALL CHESTS ---- */
        public void checkChests
           (List<Chest>  chestList,
            Player       player,
            float        timeStep)
        {
            /* Future position of the player */
            float nextHorPos, nextVerPos;

            /* Booleans telling whether the movements are possible */
            bool horAllowed = true;
            bool verAllowed = true;

            /* Propagate player's position one step ahead */
            nextHorPos = player.HorPos + player.HorSpeed * timeStep;
            nextVerPos = player.VerPos + player.VerSpeed * timeStep;

            foreach(Chest chest in chestList)
            {
                if(horAllowed == true || verAllowed == true)
                {
                    /* Check physics collisions */
                    if(basicCheck
                       (chest.PhysBox, chest.HorPos, chest.VerPos,
                        player.CollBox, nextHorPos, player.VerPos) == true)
                    {
                        horAllowed = false;
                    }
                    if(basicCheck
                       (chest.PhysBox, chest.HorPos, chest.VerPos,
                        player.CollBox, player.HorPos, nextVerPos) == true)
                    {
                        verAllowed = false;
                    }
                    if(basicCheck
                       (chest.PhysBox, chest.HorPos, chest.VerPos,
                        player.CollBox, nextHorPos, nextVerPos) == true)
                    {
                        horAllowed = false;
                        verAllowed = false;
                    }

                    /* Handle physics collisions */
                    if(horAllowed == false)
                    {
                        player.HorSpeed = 0.0f;
                    }
                    if(verAllowed == false)
                    {
                        player.VerSpeed = 0.0f;
                    }
                }

                /* Check action collision */
                if(player.IsActionPerf == true)
                {
                    if(basicCheck
                       (chest.ActBox, chest.HorPos, chest.VerPos,
                        player.CollBox, player.HorPos, player.VerPos) == true)
                    {
                        chest.OpenChest();
                        player.IsActionPerf = false;
                        Debug.Log("Chest opened");
                    } 
                }
            }
        }

        /* ---- BASIC COLLISION CHECK ---- */
        public bool basicCheck
           (ColliderBox firstBox,
            float firstHorPos_pix, float firstVerPos_pix,
            ColliderBox secondBox,
            float secondHorPos_pix, float secondVerPos_pix)
        {
            if(((firstHorPos_pix + firstBox.HorOffset + firstBox.Width/2) <=
               (secondHorPos_pix + secondBox.HorOffset - secondBox.Width/2)) ||
               ((firstHorPos_pix + firstBox.HorOffset - firstBox.Width/2) >=
               (secondHorPos_pix + secondBox.HorOffset + secondBox.Width/2)) ||
               ((firstVerPos_pix + firstBox.VerOffset + firstBox.Height/2) <=
               (secondVerPos_pix + secondBox.VerOffset - secondBox.Height/2)) ||
               ((firstVerPos_pix + firstBox.VerOffset - firstBox.Height/2) >=
               (secondVerPos_pix + secondBox.VerOffset + secondBox.Height/2)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}

